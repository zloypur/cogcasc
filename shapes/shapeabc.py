from abc import abstractmethod, ABC


class ShapeABC(ABC):
    @abstractmethod
    def __init__(self):
        pass

    @property
    @abstractmethod
    def area(self):
        pass

    @property
    @abstractmethod
    def perimeter(self):
        pass

    @property
    @abstractmethod
    def volume(self):
        pass

    @property
    @abstractmethod
    def points(self):
        pass

    def __str__(self):
        return f"{type(self)}: {{{', '.join(map(str, self.points))}}}"
