import math
from typing import NamedTuple


from shapes.shapeabc import ShapeABC
from util.point import Point2D


class Rectangle(ShapeABC):
    def __init__(self, ptl: Point2D, ptr: Point2D, pbr: Point2D, pbl: Point2D):
        if ptl.x > ptr.x:
            ptl, ptr = ptr, ptl
        if pbl.x > pbr.x:
            pbl, pbr = pbr, pbl
        if ptl.y < pbl.y:
            ptl, pbl = pbl, ptl
        if ptr.y < pbr.y:
            ptr, pbr = pbr, ptr
        if (not (89.99 <= (ptr - ptl).angle(pbl - ptl, "deg") <= 90.01)
                or not (89.99 <= (ptr - pbr).angle(pbl - pbr, "deg") <= 90.01)):
            raise ValueError("Rectangle must have right angles")
        self.ptl = ptl
        self.ptr = ptr
        self.pbr = pbr
        self.pbl = pbl

    @property
    def area(self):
        return (self.ptr - self.ptl) * (self.pbl - self.ptl)

    @property
    def perimeter(self):
        return 2 * ((self.ptr - self.ptl).norm + (self.pbl - self.ptl).norm)

    @property
    def volume(self):
        return 0

    @property
    def points(self):
        return (self.ptl.as_tuple(), self.ptr.as_tuple(),
                self.pbr.as_tuple(), self.pbl.as_tuple())

    def __str__(self):
        return f"Rectangle: {{{', '.join(map(str, self.points))}}}"
