from shapes.shapes2d.circle import Circle
from shapes.shapes2d.triangle import Triangle
from shapes.shapes2d.rectangle import Rectangle
from util.point import Point2D
