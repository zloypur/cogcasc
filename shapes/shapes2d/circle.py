import math
from typing import NamedTuple


from shapes.shapeabc import ShapeABC
from util.point import Point2D


class Circle(ShapeABC):
    def __init__(self, radius: float, center: Point2D, angle_deg_step: float = 1):
        if radius <= 0:
            raise ValueError("Radius is less or equal to zero")
        if angle_deg_step <= 0:
            raise ValueError("Discretiasation angle is less than zero")
        self.radius = radius
        self.center = center
        self.angle_deg_step = angle_deg_step

    @property
    def area(self):
        return math.pi * self.radius * self.radius / 2

    @property
    def perimeter(self):
        return 2 * math.pi * self.radius

    @property
    def volume(self):
        return 0

    @property
    def points(self):
        return tuple((Point2D(self.radius * math.cos(phi / 180.0 * math.pi),
                              self.radius * math.sin(phi / 180.0 * math.pi)) +
                      self.center).as_tuple()
                     for phi in (i*self.angle_deg_step
                                 for i in range(0, int(360//self.angle_deg_step))))

    def __str__(self):
        return f"Circle: {{radius: {self.radius}, center: {self.center}}}"
