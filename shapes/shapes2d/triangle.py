import math
from typing import NamedTuple


from shapes.shapeabc import ShapeABC
from util.point.point2d import Point2D


class Triangle(ShapeABC):
    def __init__(self, p1: Point2D, p2: Point2D, p3: Point2D):
        l12 = (p1 - p2).norm
        l23 = (p2 - p3).norm
        l31 = (p3 - p1).norm
        if l12 + l23 < l31 or l23 + l31 < l12 or l31 + l12 < l23:
            raise ValueError("Provided points don't construct triangle")
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

    @property
    def area(self):
        halfper = self.perimeter / 2
        return math.sqrt(halfper * (halfper - (self.p1 - self.p2).norm)
                         * (halfper - (self.p2 - self.p3).norm)
                         * (halfper - (self.p3 - self.p1).norm))

    @property
    def perimeter(self):
        return ((self.p1 - self.p2).norm
                + (self.p2 - self.p3).norm
                + (self.p3 - self.p1).norm)

    @property
    def volume(self):
        return 0

    @property
    def points(self):
        return (self.p1.as_tuple(), self.p2.as_tuple(), self.p3.as_tuple())

    def __str__(self):
        return f"Triangle: {{{', '.join(map(str, self.points))}}}"
