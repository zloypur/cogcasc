import multiprocessing as mp
import os
import random
from itertools import chain

from statistics import ShapeImageStatistics

import numpy as np
import tensorflow as tf
from keras import backend as K
from keras.layers import Dense, Input, Lambda, Dropout
from keras.models import Model, Sequential
from keras.optimizers import Adam
from PIL import Image


def proc_file(file_list, ticks, interval_num):
    arguments, labels = [], []
    for filename in file_list:
        # print(f"process {filename}")
        image = Image.open(filename)
        image_stat = ShapeImageStatistics(image)
        try:
            inner_chords = image_stat.get_internal_chords(
                ticks, interval_num, True)
            outer_chords = image_stat.get_internal_chords(
                ticks, interval_num, True)
        except ZeroDivisionError:
            print(f"Division by zero while processing '{filename}'")
            continue
        features = [*map(lambda x: x[1], sorted(inner_chords.items(), key=lambda x: x[0])),
                    *map(lambda x: x[1], sorted(outer_chords.items(), key=lambda x: x[0]))]
        label = os.path.splitext(os.path.basename(filename))[0].split("_")[1]
        arguments.append(features)
        labels.append(label)
    return arguments, labels


def prepare_data(base_folder, interval_num, ticks=None):

    files = [*filter(lambda x: os.path.isfile(x) and x.endswith(".png"),
                     map(lambda name: os.path.join(base_folder, name),
                         os.listdir(base_folder)))]

    thread_num = mp.cpu_count()
    pool = mp.Pool(thread_num)

    files_by_thread = len(files) // thread_num

    results = pool.starmap(
        proc_file, [(files[(i - 1)*files_by_thread:i*files_by_thread], ticks, interval_num)
                    for i in range(1, thread_num)])

    pool.close()

    if len(files) % thread_num:
        results.append(
            proc_file(files[thread_num*files_by_thread:], ticks, interval_num))

    arguments = [*chain.from_iterable(map(lambda x: x[0], results))]
    labels = [*chain.from_iterable(map(lambda x: x[1], results))]

    return np.array(arguments, dtype=np.float32), labels


def create_pairs(features, classmarks):
    pairs, labels = [], []
    for i in range(len(features)):
        for j in range(len(features)):
            if i != j:
                pairs.append([features[i], features[j]])
                labels.append(1 if classmarks[i] == classmarks[j] else 0)
    return np.array(pairs), np.array(labels)


def create_base_network(input_shape, embeding_size: int):
    models = Sequential()
    models.add(Dense(int(input_shape[0]*2),
                     activation="relu", input_shape=input_shape))
    models.add(Dense(1000, activation="relu"))
    models.add(Dense(1000,  activation="relu"))
    # models.add(Dropout(0.2))
    models.add(Dense(1000, activation="relu"))
    models.add(Dense(7500, activation="relu"))
    models.add(Dense(500, activation="relu"))
    models.add(Dense(250,  activation="relu"))
    # models.add(Dropout(0.2))
    models.add(Dense(250,  activation="relu"))
    models.add(Dense(100, activation="sigmoid"))
    return models


def euclidean_distance(vects):
    x, y = vects
    sum_square = K.sum(K.square(x - y), axis=1, keepdims=True)
    return K.sqrt(K.maximum(sum_square, K.epsilon()))


def eucl_dist_output_shape(shapes):
    shape1, _ = shapes
    return (shape1[0], 1)


def contrastive_loss(y_true, y_pred):
    margin = 1
    square_pred = K.square(y_pred)
    margin_square = K.square(K.maximum(margin - y_pred, 0))
    return K.mean(y_true * square_pred + (1 - y_true) * margin_square)


def accuracy(y_true, y_pred):
    return K.mean(K.equal(y_true, K.cast(y_pred < 0.5, y_true.dtype)))


def create_siamese_network(x_train, y_train, x_test, y_test, embeding_size) -> Model:
    tr_pairs, tr_labels = create_pairs(x_train, y_train)

    te_pairs, te_labels = create_pairs(x_test, y_test)

    input_shape = tr_pairs.shape[2:]
    base_network = create_base_network(input_shape, embeding_size)
    input_first = Input(input_shape)
    input_second = Input(input_shape)
    processed_first = base_network(input_first)
    processed_second = base_network(input_second)
    distance = Lambda(euclidean_distance, output_shape=eucl_dist_output_shape)(
        [processed_first, processed_second])

    model = Model([input_first, input_second], distance)

    model.compile(loss=contrastive_loss,
                  optimizer=Adam(), metrics=[accuracy])

    model.fit([tr_pairs[:, 0], tr_pairs[:, 1]], tr_labels,
              batch_size=128,
              epochs=1,
              validation_data=([te_pairs[:, 0], te_pairs[:, 1]], te_labels))

    return model
