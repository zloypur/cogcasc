from PIL import Image
import random
import math
from typing import Dict, Type, Tuple, TypeVar


import shape_drawers as drawers
import shapes.shapes2d as shapes
from shapes import ShapeABC
from util.point import Point2D
from util.boundingrect import BoundingRect
import shape_generators as gens


_DEFAULT_GENERATOR_TYPES = {
    shapes.Circle: gens.CircleGenerator,
    shapes.Rectangle: gens.RectangleGenerator,
    shapes.Triangle: gens.TriangleGenerator,
}

_DEFAULT_DRAWERS_TYPES = {
    shapes.Circle: drawers.CircleDrawer,
    shapes.Rectangle: drawers.RectangleDrawer,
    shapes.Triangle: drawers.TriangleDrawer,
}


class ShapeImageGenerator:
    def __init__(self, image_size, background_color, shape_color,
                 shape_generators_dict: Dict[Type[ShapeABC], gens.ShapeGeneratorABC] = None,
                 shape_drawers_dict: Dict[Type[ShapeABC], Type[drawers.ShapeDrawerABC]] = None):
        if len(image_size) != 2:
            raise ValueError("image_size len must be 2")
        self.image_size = image_size
        self.background_color = background_color
        self.shape_color = shape_color
        self.shape_generator = gens.ShapeGenerator(
            BoundingRect(0, 0, *image_size))
        if shape_generators_dict is None:
            shape_generators_dict = {
                k: v(self.shape_generator.bounding_rect)
                for k, v in _DEFAULT_GENERATOR_TYPES.items()
            }
        for shape_type, shape_gen in shape_generators_dict.items():
            self.shape_generator.register(shape_type, shape_gen)
        if shape_drawers_dict is None:
            shape_drawers_dict = _DEFAULT_DRAWERS_TYPES.copy()
        self.shape_dawers = shape_drawers_dict

    def generate_image(self, shape_type: Type[ShapeABC] = None, **kwargs) -> Tuple[Image.Image, ShapeABC]:
        if shape_type is None:
            shape_type = random.choice(self.shape_generator.registred_shapes)
        shape = self.shape_generator.generate(shape_type)
        return self.draw_shape_image(shape, **kwargs), type(shape)

    def draw_shape_image(self, shape: ShapeABC, **kwargs):
        image = Image.new("RGB", self.image_size, self.background_color)
        if type(shape) not in self.shape_dawers:
            raise ValueError(f"Unexpected shape type {type(shape)}. "
                             "Can't find drawer")
        self.shape_dawers[type(shape)](shape).draw(
            image, self.shape_color, **kwargs)
        return image

    def generate_noisy_image(self, shape_type: Type[ShapeABC] = None,
                             sup_point_per_edge=3, min_noize=0, max_noize=1,
                             **kwargs) -> Tuple[Image.Image, Type[ShapeABC]]:
        baseshape = self.shape_generator.generate(shape_type)
        #polygon = baseshape.points
        polygon = self._generate_noisy_poligon(
            baseshape, sup_point_per_edge, min_noize, max_noize)
        return self._draw_polygon(polygon, **kwargs), type(baseshape)

    def _draw_polygon(self, polygon: list, **kwargs):
        image = Image.new("RGB", self.image_size, self.background_color)
        from PIL import ImageDraw
        ImageDraw.Draw(image).polygon(polygon, self.shape_color, **kwargs)
        return image

    @staticmethod
    def _generate_noisy_poligon(baseshape: ShapeABC, sup_point_per_edge=3,
                                min_noize=0, max_noize=1) -> list:
        newpoints = []
        points = baseshape.points
        for p1, p2 in zip([points[-1], *points[:-1]], points):
            dir_vect = Point2D(*p2) - Point2D(*p1)
            dir_len = dir_vect.norm
            dir_vect = dir_vect / dir_len
            newpoints.append(p1)
            for i in map(lambda x: (x + 1) * dir_len/(1 + sup_point_per_edge),
                         range(sup_point_per_edge)):
                k = random.random() * (max_noize - min_noize) + min_noize
                normal = Point2D(-dir_vect.y, dir_vect.x)
                newpoints.append(
                    ((normal * k) + p1 + (dir_vect * i)).as_tuple())
            newpoints.append(p2)
        return newpoints
