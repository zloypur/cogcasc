from abc import abstractmethod, ABC
from typing import Callable
from random import random


from util.boundingrect import BoundingRect


class ShapeGeneratorABC(ABC):
    def __init__(self, bounding_rect: BoundingRect, rand_gen: Callable[[None], float] = random):
        self.bounding_rect = bounding_rect
        self.rand_gen = rand_gen

    @abstractmethod
    def generate(self):
        pass
