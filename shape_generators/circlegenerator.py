from shape_generators import ShapeGeneratorABC
from typing import Callable
from random import random


from util.boundingrect import BoundingRect
from util.point import Point2D
from shapes.shapes2d import Circle


class CircleGenerator(ShapeGeneratorABC):
    def __init__(self, bounding_rect: BoundingRect, rand_gen: Callable[[None], float] = random):
        super().__init__(bounding_rect, rand_gen)

    def generate(self):
        rlim = (min(self.bounding_rect.width, self.bounding_rect.height) / 2)
        r = self.rand_gen() * rlim + 1
        cpx = self.rand_gen() * (self.bounding_rect.width - 2 * r) + \
            r + self.bounding_rect.pbl.x
        cpy = self.rand_gen() * (self.bounding_rect.height - 2 * r) + \
            r + self.bounding_rect.pbl.y
        desc_step = self.rand_gen() * 45
        return Circle(r, Point2D(cpx, cpy), desc_step)
