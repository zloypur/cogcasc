from shape_generators import ShapeGeneratorABC
from typing import Callable
from random import random


from util.boundingrect import BoundingRect
from util.point import Point2D
from shapes.shapes2d import Triangle


class TriangleGenerator(ShapeGeneratorABC):
    def __init__(self, bounding_rect: BoundingRect, rand_gen: Callable[[None], float] = random):
        super().__init__(bounding_rect, rand_gen)

    def generate(self):
        p1, p2, p3 = [Point2D(self.rand_gen() * self.bounding_rect.width,
                              self.rand_gen() * self.bounding_rect.height)
                      for _ in range(3)]
        return Triangle(p1, p2, p3)
