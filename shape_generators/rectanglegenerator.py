from shape_generators import ShapeGeneratorABC
from typing import Callable
from random import random


from util.boundingrect import BoundingRect
from util.point import Point2D
from shapes.shapes2d import Rectangle


class RectangleGenerator(ShapeGeneratorABC):
    def __init__(self, bounding_rect: BoundingRect, rand_gen: Callable[[None], float] = random):
        super().__init__(bounding_rect, rand_gen)

    def generate(self):
        rlim = min(self.bounding_rect.width, self.bounding_rect.height) / 4
        p1, p3 = [Point2D(self.rand_gen() * (self.bounding_rect.width - rlim) + rlim / 2,
                          self.rand_gen() * (self.bounding_rect.height - rlim) + rlim / 2)
                  for _ in range(2)]

        dir_vect = p3 - p1
        normal = Point2D(-dir_vect.y, dir_vect.x)
        normal = normal / normal.norm

        k = self.rand_gen() * rlim / 2 + rlim // 10

        p2 = p1 + normal * k
        p4 = p3 + normal * k

        return Rectangle(p1, p2, p3, p4)
