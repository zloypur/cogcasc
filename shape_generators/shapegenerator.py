import threading
from typing import Callable, Type
from random import choice


from shape_generators import ShapeGeneratorABC, CircleGenerator, RectangleGenerator, TriangleGenerator
from shapes import ShapeABC
from shapes.shapes2d import Circle, Rectangle, Triangle
from util.boundingrect import BoundingRect


class ShapeGenerator(ShapeGeneratorABC):
    def __init__(self, bounding_rect: BoundingRect):
        super().__init__(bounding_rect)
        self.shape_generators = {}

    def register(self, shape_type: Type[ShapeABC], generator: ShapeGeneratorABC):
        if self.bounding_rect != generator.bounding_rect:
            raise ValueError("Bounding rects must be equal")
        if shape_type in self.shape_generators:
            raise ValueError("Type generator is already registred")
        self.shape_generators[shape_type] = generator

    @property
    def registred_shapes(self):
        return list(self.shape_generators.keys())

    @property
    def registred_generators(self):
        return list(self.shape_generators.values())

    def get_generator(self, shape_type: Type[ShapeABC] = None):
        if shape_type not in self.shape_generators:
            raise KeyError(
                f"Provided shape type {shape_type} not presented in collection")
        return self.shape_generators[shape_type]

    def generate(self, shape_type: Type[ShapeABC] = None):
        if not self.shape_generators:
            raise KeyError("Shape generators collection is empty")
        if shape_type is None:
            shape_type = choice(list(self.shape_generators.keys()))
        if shape_type in self.shape_generators:
            return self.shape_generators[shape_type].generate()
        raise KeyError(
            f"Provided shape type {shape_type} not presented in collection")
