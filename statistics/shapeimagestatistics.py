import numpy as np
from PIL import Image
from collections import Counter


class ShapeImageStatistics:
    def __init__(self, image: Image.Image):
        width, height = image.size
        if height != width:
            raise ValueError("Image must be square.")
        self.raw_data: np.ndarray = np.asanyarray(image)
        self.image_shape = tuple(self.raw_data.shape[:2])

    def to_grayscale(self, inverse=False) -> np.ndarray:
        return np.asanyarray(
            [
                [
                    sum(self.raw_data[i, j, :3]) / 3 if not inverse
                    else 255.0 - sum(self.raw_data[i, j, :3]) / 3
                    for j in range(self.image_shape[1])
                ]
                for i in range(self.image_shape[0])
            ]
        )

    def get_outer_chords(self, ticks=None, interval_num=None, normalize=True) -> Counter:
        width, height = self.image_shape
        if not ticks:
            ticks = height
        bnw_image = self.to_grayscale(inverse=True)
        statistic = []
        for i in range(0, height, height // ticks):
            left, rignt, top, bot = None, None, None, None
            for j in range(0, width, width // ticks):
                if left and rignt and top and bot:
                    break
                if not left and bnw_image[i, j]:
                    left = j
                if not rignt and bnw_image[i, width - j - 1]:
                    rignt = width - j
                if not top and bnw_image[j, i]:
                    top = j
                if not rignt and bnw_image[height - j - 1, i]:
                    rignt = height - j - 1
            for v in left, rignt, top, bot:
                if not v:
                    v = width
                statistic.append(v)
        res = Counter(statistic)
        if interval_num:
            res = self.reduce_statistics(res, interval_num)
        if normalize:
            res = self.normalize_statistics(res)
        return res

    def get_internal_chords(self, ticks=None, interval_num=None, normalize=True) -> Counter:
        width, height = self.image_shape
        if not ticks:
            ticks = height
        bnw_image = self.to_grayscale(inverse=True)
        statistic = []
        step = height // ticks
        for i in range(0, height, step):
            horizontal_internal_chord = 0
            vertical_internal_chord = 0
            for j in range(0, width, step):
                if bnw_image[i, j]:
                    horizontal_internal_chord += step
                elif horizontal_internal_chord:
                    statistic.append(horizontal_internal_chord)
                    horizontal_internal_chord = 0
                if bnw_image[j, i]:
                    vertical_internal_chord += step
                elif vertical_internal_chord:
                    statistic.append(vertical_internal_chord)
                    vertical_internal_chord = 0
        res = Counter(statistic)
        if interval_num:
            res = self.reduce_statistics(res, interval_num)
        if normalize:
            res = self.normalize_statistics(res)
        return res

    @staticmethod
    def reduce_statistics(statistic: Counter, interval_num) -> Counter:
        max_val = max(statistic.keys())
        min_val = min(statistic.keys())
        int_val = (max_val - min_val) / interval_num
        reduced_stat = Counter(
            {i * int_val: 0 for i in range(interval_num + 1)})
        for key, val in statistic.items():
            cur_int = ((key - min_val) // int_val) * int_val
            reduced_stat[cur_int] += val
        return reduced_stat

    @staticmethod
    def normalize_statistics(statistic: Counter) -> Counter:
        sum_ = sum(statistic.values())
        return Counter({key: val/sum_ for key, val in statistic.items()})
