import math
from typing import Union, Tuple, NamedTuple

from util.point.pointabc import PointABC


class Point2D(PointABC):
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    @property
    def norm(self):
        return math.sqrt(self.x*self.x + self.y*self.y)

    def as_tuple(self):
        return self.x, self.y

    def angle(self, other: PointABC, units: str = "rad"):
        zero_point = Point2D(0, 0)
        if self == zero_point or other == zero_point:
            raise ValueError("Angle to zero vector could not be found")
        angle = math.acos(self * other / self.norm / other.norm)
        if units == "rad":
            return angle
        if units == "deg":
            return angle * 180.0 / math.pi
        raise ValueError(f"Unexpected units type: {units}")

    def __add__(self, other: Union[PointABC, Tuple]) -> PointABC:
        if issubclass(type(other), Point2D):
            return Point2D(self.x + other.x, self.y + other.y)
        if issubclass(type(other), PointABC):
            if len(other) > 2:
                return other + self
            else:
                other = other.as_tuple()
        if issubclass(type(other), Tuple):
            if len(other) < 2:
                raise ValueError("Other point has less than 2 arguments")
            return Point2D(self.x + other[0], self.y + other[1])
        raise NotImplementedError()

    def __sub__(self, other: Union[PointABC, Tuple]) -> PointABC:
        if issubclass(type(other), Point2D):
            return Point2D(self.x - other.x, self.y - other.y)
        if issubclass(type(other), PointABC):
            if len(other) > 2:
                return -(other - self)
            else:
                other = other.as_tuple()
        if issubclass(type(other), Tuple):
            if len(other) < 2:
                raise ValueError("Other point has less than 2 arguments")
            return Point2D(self.x - other[0], self.y - other[1])
        raise NotImplementedError()

    def __mul__(self, other: Union[PointABC, float, int, Tuple]) -> Union[float, PointABC]:
        if issubclass(type(other), Point2D):
            return self.x*other.x + self.y*other.y
        if isinstance(other, (int, float)):
            return Point2D(self.x * other, self.y * other)
        raise NotImplementedError()

    def __truediv__(self, other: Union[float, int]) -> PointABC:
        if other == 0:
            raise ArithmeticError("Division vector by zero")
        return Point2D(self.x / other, self.y / other)

    def __neg__(self):
        return Point2D(-self.x, -self.y)

    def __len__(self):
        return 2

    def __eq__(self, other):
        if issubclass(type(other), Point2D):
            return self.x == other.x and self.y == other.y
        return super().__eq__(self, other)

    def __ne__(self, other):
        return not self == other

    def __str__(self):
        return f"(x: {self.x}, y: {self.y})"
