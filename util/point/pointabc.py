from abc import abstractmethod, ABC
from typing import Union, Tuple


class PointABC(ABC):
    def __init__(self):
        pass

    @property
    @abstractmethod
    def norm(self):
        pass

    @abstractmethod
    def as_tuple(self):
        pass

    @abstractmethod
    def angle(self, other, units):
        pass

    @abstractmethod
    def __len__(self):
        pass

    @abstractmethod
    def __neg__(self):
        pass

    @abstractmethod
    def __mul__(self, other):
        pass

    @abstractmethod
    def __truediv__(self, other):
        pass

    @abstractmethod
    def __add__(self, other):
        pass

    @abstractmethod
    def __sub__(self, other):
        pass

    def __eq__(self, other):
        if isinstance(other, tuple):
            return self.as_tuple() == other
        return self.as_tuple() == other.as_tuple()

    def __ne__(self, other):
        return not self == other

    def __str__(self):
        return str(self.as_tuple())

    def __abs__(self):
        return self.norm
