from typing import NamedTuple


from util.point import Point2D


class BoundingRect:
    def __init__(self, x1, y1, x2, y2):
        self.ptr = Point2D(max(x1, x2), max(y1, y2))
        self.pbl = Point2D(min(x1, x2), min(y1, y2))

    def __eq__(self, other):
        return self.ptr == other.ptr and self.pbl == other.pbl

    def __ne__(self, other):
        return not self == other

    @property
    def height(self):
        return self.ptr.y - self.pbl.y

    @property
    def width(self):
        return self.ptr.x - self.pbl.x
