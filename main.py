import os
import shutil
import pickle
from collections import Counter
from random import random

import numpy as np

from image_generator import ShapeImageGenerator
from shapes.shapes2d import Circle, Rectangle, Triangle
from siamese_network import create_siamese_network, prepare_data

# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

GENERATE_DATA = False


def generate_images(folder, shape_names, image_shape, back_col, shape_col, images_cnt, noisy=None, **kwargs):
    gen = ShapeImageGenerator(image_shape, back_col, shape_col)
    for i in range(images_cnt):
        if noisy == "semi":
            if random() > 0.7:
                image, shape_type = gen.generate_noisy_image(
                    shape_type=None, **kwargs)
            else:
                image, shape_type = gen.generate_image()
        elif noisy == "full":
            image, shape_type = gen.generate_noisy_image(
                shape_type=None, **kwargs)
        else:
            image, shape_type = gen.generate_image()
        image.save(os.path.join(folder, f"{i}_{shape_names[shape_type]}.png"))


if __name__ == "__main__":

    shape = (100, 100)
    back_col = (255, 255, 255)
    shape_col = (0, 0, 0)
    shape_names = {
        Circle: "circle",
        Rectangle: "rectangle",
        Triangle: "triangle"
    }
    images_cnt = 1000

    if GENERATE_DATA:
        shutil.rmtree(".pics/train")
        os.mkdir(".pics/train")
        generate_images(".pics/train", shape_names, shape,
                        back_col, shape_col, images_cnt,
                        noisy='semi', sup_point_per_edge=2,
                        min_noize=-2, max_noize=5)
        x_data, y_data = prepare_data(".pics/train", 60)
        with open(".data.train", "wb") as outfile:
            pickle.dump((x_data, y_data), outfile)
    else:
        with open(".data.train", "rb") as inputfile:
            x_data, y_data = pickle.load(inputfile)

    train_pers = 0.7
    train_cnt = int(len(x_data) * train_pers)

    x_train, y_train = x_data[:train_cnt], y_data[:train_cnt]

    x_test, y_test = x_data[train_cnt:], y_data[train_cnt:]

    sn = create_siamese_network(x_train, y_train, x_test, y_test, 30)

    if GENERATE_DATA:
        shutil.rmtree(".pics/test")
        os.mkdir(".pics/test")
        generate_images(".pics/test", shape_names, shape,
                        back_col, shape_col, images_cnt,
                        noisy="semi", sup_point_per_edge=2,
                        min_noize=-2, max_noize=5)
        x_data, y_data = prepare_data(".pics/test", 60)
        with open(".data.test", "wb") as outfile:
            pickle.dump((x_data, y_data), outfile)
    else:
        with open(".data.test", "rb") as inputfile:
            x_data, y_data = pickle.load(inputfile)

    success = 0
    for i in range(len(x_data)):
        x = x_data[i]
        y = y_data[i]
        pairs = np.array([[x, tr] for tr in x_train])
        predictions = sn.predict([pairs[:, 0], pairs[:, 1]])
        lables = {}
        for j, pred in enumerate(predictions):
            if y_train[j] not in lables:
                lables[y_train[j]] = 0
            lables[y_train[j]] += pred[0]
        c = Counter(lables)
        sum_ = sum(c.values())
        c = {key: val/sum_ for key, val in c.items()}
        if c:
            label = sorted(c.items(), key=lambda x: x[1])[0][0]
            if label == y:
                success += 1
            # print(
            #     f"True type: '{y}'. Predicted type: '{label}'. Prediction: {c}")
        else:
            print(f"True type: '{y}'. Prediction: 'undefined type'")
    print(f"Accuracy: {success/len(x_data)}")
