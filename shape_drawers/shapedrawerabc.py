from abc import abstractmethod, ABC
from typing import Union, Type, Tuple
from PIL import Image


from shapes import ShapeABC


class ShapeDrawerABC(ABC):
    def __init__(self, shapetype, shape: ShapeABC = None, **kwargs):
        self.shape = None
        if shape is None and issubclass(shapetype, ShapeABC):
            self.shape = shapetype(**kwargs)
        if self.shape is None and isinstance(shape, shapetype):
            self.shape = shape
        if self.shape is None:
            raise ValueError(
                "shape parametr must be either a shape or shape type")

    @abstractmethod
    def draw(self, image: Image, color: Tuple[int], **kwargs):
        pass
