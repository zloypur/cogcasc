from PIL import Image, ImageDraw
from typing import Union, Type, Tuple


from shape_drawers import ShapeDrawerABC
from shapes.shapes2d import Circle


class CircleDrawer(ShapeDrawerABC):
    def __init__(self, shape: Union[Type, Circle], **kwargs):
        if isinstance(shape, type):
            super().__init__(Circle, None, kwargs)
        else:
            super().__init__(Circle, shape)

    def draw(self, image: Image, color: Tuple[int], **kwargs):
        ImageDraw.Draw(image).ellipse([
            (self.shape.center.x - self.shape.radius,
             self.shape.center.y - self.shape.radius),
            (self.shape.center.x + self.shape.radius,
             self.shape.center.y + self.shape.radius),
        ], color, **kwargs)
