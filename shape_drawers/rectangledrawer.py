from PIL import Image, ImageDraw
from typing import Union, Type, Tuple


from shape_drawers import ShapeDrawerABC
from shapes.shapes2d import Rectangle


class RectangleDrawer(ShapeDrawerABC):
    def __init__(self, shape: Union[Type, Rectangle], **kwargs):
        if isinstance(shape, type):
            super().__init__(Rectangle, None, kwargs)
        else:
            super().__init__(Rectangle, shape)

    def draw(self, image: Image, color: Tuple[int], **kwargs):
        ImageDraw.Draw(image).polygon(self.shape.points, color, **kwargs)
