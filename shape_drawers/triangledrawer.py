from PIL import Image, ImageDraw
from typing import Union, Type, Tuple


from shape_drawers import ShapeDrawerABC
from shapes.shapes2d import Triangle


class TriangleDrawer(ShapeDrawerABC):
    def __init__(self, shape: Union[Type, Triangle], **kwargs):
        if isinstance(shape, type):
            super().__init__(Triangle, None, kwargs)
        else:
            super().__init__(Triangle, shape)

    def draw(self, image: Image, color: Tuple[int], **kwargs):
        ImageDraw.Draw(image).polygon(self.shape.points, color, **kwargs)
